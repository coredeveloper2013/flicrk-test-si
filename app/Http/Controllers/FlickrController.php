<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FlickrController extends Controller
{
    public function scrapImage(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=5baab9a974236205f53e4f8bf4a02bbf&user_id=161100420%40N02&format=json&nojsoncallback=1&auth_token=72157701486966782-4646f9d4558eb383&api_sig=3e67fac0cbae4ac8c2d0c9a0c71f056b'
        ));
        $resp = curl_exec($curl);
        $data = json_decode($resp);
        $photos = $data->photos->photo;

        /*
            pulling image link
            https://farm".$farmID.".staticflickr.com/".$serverID."/".$photoID."_".$secretID."_".$size.".jpg"
        */

        foreach($photos as $photo){
            $imageLink[] = sprintf("https://farm%s.staticflickr.com/%s/%s_%s_b.jpg",$photo->farm,$photo->server,$photo->id,$photo->secret);
        }
        curl_close($curl);

        return response()->json([
            'images'=>$imageLink
        ],200);
    }
}
